﻿(function () {
    $.datepicker.setDefaults({
        firstDay: 1,
        showWeek: true,
        dateFormat: "dd/mm/yy"
    });

    var pds = angular.module("PDS", []);

    pds.controller("ctrlPDS", ["$scope", "$http", function ($scope, $http) {
        $("#Current").on({
            change: debounce(function () {
                $scope.data.Date = getDate();

                getEvents();
            }, 300)
        }).datepicker();

        var Current = document.querySelector("#Current");

        $scope.data = {
            Date: getDate(),

            Events: [],
            loadingEvents: true,
            error: false,

            ModalEvent: null
        };

        $scope.updateModal = function (id) {
            var event = $scope.data.Events.find(function (evt) {
                return evt.ID === id;
            });

            // Set ModalEvent to the found event or null. If null, display an error on the modal.
            $scope.data.ModalEvent = event || null;
        };

        function getDate() {
            var date = $("#Current").datepicker("getDate");

            return date || new Date();
        }

        function getEvents() {
            $scope.data.loadingEvents = true;
            $scope.data.error = false;

            $http.get(Current.dataset.eventsUrl, { params: { weekBeginning: $scope.data.Date.toISOString() } })
                .then(function (result) {
                    if (result && result.data && result.data.length) {
                        $scope.data.Events = result.data;
                    }
                    else {
                        $scope.data.Events = [];
                    }
                }, function () {
                    $scope.data.error = true;
                }).finally(function () {
                    $scope.data.loadingEvents = false;
                });
        }

        getEvents();
    }]);

    // Borrowed from https://davidwalsh.name/javascript-debounce-function
    // Returns a function, that, as long as it continues to be invoked, will not
    // be triggered. The function will be called after it stops being called for
    // N milliseconds. If `immediate` is passed, trigger the function on the
    // leading edge, instead of the trailing.
    function debounce(func, wait, immediate) {
        var timeout;
        return function () {
            var context = this, args = arguments;
            var later = function () {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };
})();