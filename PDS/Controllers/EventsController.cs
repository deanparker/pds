﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PDS.Services;
using PDS.Models;

namespace PDS.Controllers
{
    public class EventsController : ApiController
    {
        // GET api/<controller>
        public IHttpActionResult Get(DateTime weekBeginning)
        {
            var service = new DataService();

            var events = service.GetEvents(weekBeginning).Select(sel => new EventModel()
            {
                ID = sel.ID,
                StartDate = sel.StartDate,
                EndDate = sel.EndDate,
                StartTime = sel.StartTime,
                EndTime = sel.EndTime,
                Description = sel.Description,
                Category = sel.Category,
                Members = sel.Members.Select(s =>
                {
                    var member = service.GetMember(s.ID);

                    return new MemberModel()
                    {
                        ID = member.ID,
                        FullTitle = member.FullTitle,
                        MemberFrom = member.MemberFrom,
                        Party = member.Party
                    };
                })
            }).OrderBy(ord => ord.StartDate);

            return Ok(events);
        }
    }
}