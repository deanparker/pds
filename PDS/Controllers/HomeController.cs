﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PDS.Models;
using PDS.Services;

namespace PDS.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var date = DateTime.Today;

            // Cycle back until we reach Monday of this week.
            while(date.DayOfWeek != DayOfWeek.Monday)
            {
                date = date.AddDays(-1);
            }

            var model = new EventViewModel()
            {
                Current = date
            };
            
            return View(model);
        }
    }
}