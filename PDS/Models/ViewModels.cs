﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PDS.Models
{
    public class EventViewModel
    {
        [DisplayName("Week beginning")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public DateTime Current { get; set; }
    }

    public class EventModel
    {
        public int ID { get; set; }
        public DateTime StartDate { get; set; }
        public string StartTime { get; set; }
        public DateTime? EndDate { get; set; }
        public string EndTime { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }

        public IEnumerable<MemberModel> Members { get; set; }
    }

    public class MemberModel
    {
        public int ID { get; set; }
        public string Party { get; set; }
        public string MemberFrom { get; set; }
        public string FullTitle { get; set; }
    }
}