﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PDS.Services;

namespace PDS.Tests
{
    [TestClass]
    public class DataServiceTest
    {
        [TestMethod]
        public void TestGetEvents()
        {
            var dataService = new DataService();
            
            var events = dataService.GetEvents(new DateTime(2017, 4, 24));

            Assert.IsNotNull(events);
        }

        [TestMethod]
        public void TestGetMembers()
        {
            var dataService = new DataService();

            var member = dataService.GetMember(500);
            string party = "Labour";

            Assert.IsNotNull(member);
            Assert.AreEqual(party, member.Party);
        }
    }
}
