﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PDS.Domain.Event
{
    public class Event
    {
        [XmlAttribute(AttributeName = "Id")]
        public int ID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Description { get; set; }
        public int SortOrder { get; set; }
        public string Type { get; set; }
        public string House { get; set; }
        public string Category { get; set; }

        public Member[] Members { get; set; }
    }

    public class Member
    {
        [XmlAttribute(AttributeName = "Id")]
        public int ID { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
    }
}

namespace PDS.Domain.Member
{
    public class Members
    {
        public Member Member { get; set; }
    }

    public class Member
    {
        [XmlAttribute(AttributeName = "Member_Id")]
        public int ID { get; set; }
        public string FullTitle { get; set; }
        public string Party { get; set; }
        public string MemberFrom { get; set; }
    }
}