﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Xml.Serialization;
using PDS.Domain.Event;
using PDS.Domain.Member;

namespace PDS.Services
{
    public class DataService
    {
        private const string BASE_EVENTS_URL = "http://service.calendar.parliament.uk/calendar/events/list.xml";
        private const string BASE_MEMBER_URL = "http://data.parliament.uk/membersdataplatform/services/mnis/members/query";

        public IEnumerable<Event> GetEvents(DateTime weekBeginning)
        {
            // Perverse errors when using XML classes and adding a service reference not an option.
            // So read the text in manually...
            using (var client = new System.Net.WebClient())
            {
                string uri = $"{BASE_EVENTS_URL}?house=Commons&startdate={weekBeginning:yyyy-MM-dd}&enddate={weekBeginning.AddDays(6):yyyy-MM-dd}";
                
                using (var reader = new StreamReader(client.OpenRead(uri)))
                {
                    var serialiser = new XmlSerializer(typeof(Event[]));
                    IEnumerable<Event> events = (Event[])serialiser.Deserialize(reader);

                    events = events.Where(e => e.Type == "Main Chamber");

                    return events;
                }
            }
        }

        public Domain.Member.Member GetMember(int id)
        {
            string uri = $"{BASE_MEMBER_URL}/id={id}/";

            var doc = XDocument.Load(uri);

            var serialiser = new XmlSerializer(typeof(Members));
            var members = (Members)serialiser.Deserialize(doc.Root.CreateReader());

            return members.Member;
        }
    }
}